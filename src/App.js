import React, {Component} from 'react';
import './App.css'
import logo from './logo.png'
import './Progress.scss'

//below css object i used for images inside the cards

const imgclass = {
  height:'210px',
  width:'173px',
  marginTop:'-36px'
}

//data format i used

const dashboardData = [{
  name:'Sri Krishna',
  designation:'Administrator',
  email:'sriKrishna@mahabaratha.com',
  phone:1234567890,
  per:70,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/17d3942e-ea87-48af-9961-744b59e9b12b/d6bgg7j-fa5c0fb1-bb59-41b3-ba3f-ad7cfe7c9e47.jpg/v1/fill/w_600,h_800,q_75,strp/kesava_1_by_molee_d6bgg7j-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9ODAwIiwicGF0aCI6IlwvZlwvMTdkMzk0MmUtZWE4Ny00OGFmLTk5NjEtNzQ0YjU5ZTliMTJiXC9kNmJnZzdqLWZhNWMwZmIxLWJiNTktNDFiMy1iYTNmLWFkN2NmZTdjOWU0Ny5qcGciLCJ3aWR0aCI6Ijw9NjAwIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.mOCl6b4F2yabqVlKE9pGCHH5H_r9-ycHSQXVs0NjdDI'
},{
  name:'Dhrona',
  designation:'Teacher',
  email:'dronatheteacher@mahabaratha.com',
  phone:1234567890,
  per:25,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/17d3942e-ea87-48af-9961-744b59e9b12b/d6n2afv-f75f3ab5-8069-4b5c-95e1-29728a1532c4.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzE3ZDM5NDJlLWVhODctNDhhZi05OTYxLTc0NGI1OWU5YjEyYlwvZDZuMmFmdi1mNzVmM2FiNS04MDY5LTRiNWMtOTVlMS0yOTcyOGExNTMyYzQuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.kqAInS1cA0uUM68oiuBuhofvxZDxArx1JSj_73qoo5E'
},
{
  name:'Bhishma',
  designation:'Senior Member',
  email:'gangasutha@mahabaratha.com',
  phone:1234567890,
  per:90,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/17d3942e-ea87-48af-9961-744b59e9b12b/d6o46o7-678d9ac1-8864-44f0-afe1-bc18158b873f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzE3ZDM5NDJlLWVhODctNDhhZi05OTYxLTc0NGI1OWU5YjEyYlwvZDZvNDZvNy02NzhkOWFjMS04ODY0LTQ0ZjAtYWZlMS1iYzE4MTU4Yjg3M2YuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.JZiYRvpYEWAymOHf5cjnY_Y0qv9I6LDXe2c2Hd5eJDA'
},
{
  name:'Arjuna',
  designation:'Warrior & Middle Pandava',
  email:'arjunathepandava@mahabaratha.com',
  phone:1234567890,
  per:100,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/17d3942e-ea87-48af-9961-744b59e9b12b/d68qzqd-ee63c4a6-8cc8-4c4f-afb0-47a0331de1ee.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzE3ZDM5NDJlLWVhODctNDhhZi05OTYxLTc0NGI1OWU5YjEyYlwvZDY4cXpxZC1lZTYzYzRhNi04Y2M4LTRjNGYtYWZiMC00N2EwMzMxZGUxZWUuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.iY9iYwcUQ0hqQY0lwZDoxVfMRiKNygboEcTIgFCP23Y'
},{
  name:'Bheema',
  designation:'Cook & Second Pandava',
  email:'bheemathecook@mahabaratha.com',
  phone:1234567890,
  per:30,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/17d3942e-ea87-48af-9961-744b59e9b12b/d6baefr-ae2ae25e-04b1-414c-b4cb-2e4d8590143f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzE3ZDM5NDJlLWVhODctNDhhZi05OTYxLTc0NGI1OWU5YjEyYlwvZDZiYWVmci1hZTJhZTI1ZS0wNGIxLTQxNGMtYjRjYi0yZTRkODU5MDE0M2YuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.ERXQt6ZvQdXGQyjZyyZg_fXI2H1nWo9Y6mpAUkPSEow'
},{
  name:'Karna',
  designation:'Daanashoora',
  email:'thedaanashoora@mahabaratha.com',
  phone:1234567890,
  per:86,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/299ef062-7f3d-4c44-be11-dfa0f956de1d/d7tmadq-d544074d-d561-4743-aa8b-01264c5c0834.jpg/v1/fill/w_1024,h_724,q_75,strp/karna___maha_yodha_by_jubjubjedi_d7tmadq-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NzI0IiwicGF0aCI6IlwvZlwvMjk5ZWYwNjItN2YzZC00YzQ0LWJlMTEtZGZhMGY5NTZkZTFkXC9kN3RtYWRxLWQ1NDQwNzRkLWQ1NjEtNDc0My1hYThiLTAxMjY0YzVjMDgzNC5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.3nTH8WFjtOHqNnDaNKy188tjM5qUGY6HOgGpHK3rH2Q'
},
{
  name:'Shakuni',
  designation:'Playboy',
  email:'legendinludo@mahabaratha.com',
  phone:1234567890,
  per:98,
  imageurl:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/17d3942e-ea87-48af-9961-744b59e9b12b/d6voyz9-59e54023-ee69-4a82-8924-cac8a9aa2891.jpg/v1/fill/w_400,h_539,q_75,strp/shakuni___the_sorcerer_king_of_gandhara_by_molee_d6voyz9-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NTM5IiwicGF0aCI6IlwvZlwvMTdkMzk0MmUtZWE4Ny00OGFmLTk5NjEtNzQ0YjU5ZTliMTJiXC9kNnZveXo5LTU5ZTU0MDIzLWVlNjktNGE4Mi04OTI0LWNhYzhhOWFhMjg5MS5qcGciLCJ3aWR0aCI6Ijw9NDAwIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.Ehh6_8c1RYjTkPA6PLClWe_8nXTif0y_eFI6Pb65Bnc'
},
{
  name:'Duryodana',
  designation:'Kourava',
  email:'thehatavadikourava@mahabaratha.com',
  phone:1234567890,
  per:15,
  imageurl:'https://www.filmibeat.com/img/2017/07/28-1501184180-darshan4.jpg'
},
]


class App extends Component{
  constructor(props){
    super(props)
    this.state = {

    }
  }


  render(){
    var dashboardContent = dashboardData.map((data,index) => {
      return <div className='col-lg-3 col-md-4 col-sm-6 col-12'>
              <div className="card text-center" style={{maxWidth:'25rem', maxHeight:'30rem'}}>
                <div className="card-header bg-transparent" style={{borderBottom:'none'}}>
                  <div className='row'> 
                  <div className='col-3'>
                    <span> <i className="fa fa-ellipsis-v" aria-hidden="true"></i> </span>
                  </div>
                  <div className='col-9' style={{textAlign:'right'}}>
                  <i className="fa fa-star-o fa-1x" style={{color:'orange'}}></i>
                  </div>
                  </div>
                </div>
                <div className="card-body">
                  <div className="progress-pie-chart" data-percent={data.per} style={{marginTop:'-30px'}}>
                    <div className="ppc-progress">
                      <div className="ppc-progress-fill"></div>
                    </div>
                    <div className="ppc-percents">
                      <div className="pcc-percents-wrapper">
                      <img src={`${data.imageurl}`} style={imgclass} alt={data.name} />
                      </div>
                    </div>
                  </div>
                  <h2>{data.name}</h2>
                  <p>{data.designation}</p>
                </div>
                <div className="card-footer bg-transparent" style={{borderTop:'none'}}>
                  <div className='row'> <div className='col-2'> <span><i className="fa fa-envelope-o"></i></span> </div> <div className='col-10'> <p style={{textAlign:'right'}}>{data.email}</p> </div></div>
                  <div className='row'> <div className='col-2'> <span><i className="fa fa-phone"></i></span> </div> <div className='col-10'> <p style={{textAlign:'right'}}>{data.phone}</p> </div></div>
                </div>
              </div>
            </div>
    })
    
    return(
      <div>
        <nav className="navbar navbar-expand-lg navbar-light">
        <span className="navbar-brand" href="#"></span>
          <button className="navbar-toggler" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <img src="https://www.filmibeat.com/img/2017/07/28-1501184180-darshan4.jpg" width="30" height="30"  className="rounded-circle" alt="Login User"/>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active" style={{marginLeft:'100px'}}>
                <span className="nav-link navbarHeading"> Dashboard </span>
              </li>
            </ul>
            <span className="navbar-text">
              <img src="https://www.filmibeat.com/img/2017/07/28-1501184180-darshan4.jpg" width="60" height="60"  className="rounded-circle" alt="Login User"/>
            </span>
          </div>
        </nav>

        
        <div className="sidebar">
          <img src={logo} style={{marginLeft:'10px'}} alt='Logo'/>
          <ul>
            <li><i className="fa fa-window-maximize"></i></li>
            <li> <i className='fa fa-edit'></i></li>
            <li> <i className='fa fa-calendar'></i></li>
            <li> <i className='fa  fa-institution'></i></li>
            <li> <i className='fa  fa-table'></i></li>
            <li><i className="fa fa-fw fa-home"></i></li>
          </ul>
        </div>
         <div className='bg-light'>
            <div className='row no-gutters' style={{marginLeft:'130px'}}> 
            {dashboardContent}
            </div>
        </div>
      </div>
    )
  }
}

export default App;
